package com.turvo.android;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.turvo.android.bottomsheet.BottomSheetBehaviorWithAnchor;

import io.fabric.sdk.android.Fabric;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static int PERMISSIONS_REQUEST_ACCESS_LOCATION = 0x2342;

    private static final float DIM_START_OFFSET = 0.4f;

    private static final int LOCATION_UPDATE_INTERVAL = 5000;
    private static final int LOCATION_UPDATE_FASTEST_INTERVAL = 3000;

    private GoogleMap map;
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;

    private FrameLayout dimmedFrame;
    private TextView title;
    private View toolbarGroup;
    private View bottomSheet;
    private BottomSheetBehaviorWithAnchor behavior;

    private int anchorHeight;
    private int titleRightMargin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        dimmedFrame = (FrameLayout) findViewById(R.id.dimmed_frame);
        title = (TextView) findViewById(R.id.title);
        toolbarGroup = findViewById(R.id.toolbar_group);
        bottomSheet = findViewById(R.id.bottom_sheet);

        anchorHeight = getResources().getDimensionPixelSize(R.dimen.anchor_point);
        titleRightMargin = getResources().getDimensionPixelSize(R.dimen.actionbarSize) +
                getResources().getDimensionPixelSize(R.dimen.card_margin);

        initBottomsheetBehavior();
        findViewById(R.id.up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                behavior.setState(BottomSheetBehaviorWithAnchor.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                if (!requestPermissionsIfRequired()) {
                    enableCurrentLocation();
                }
                map.setOnCameraChangeListener(null);
            }
        });
    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    public void onConnected(@Nullable Bundle bundle) {
        if (checkLocationAccessEnabled()) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null) {
                moveToCurrentLocation(lastLocation);
            }

            locationRequest = new LocationRequest();
            locationRequest.setInterval(LOCATION_UPDATE_INTERVAL);
            locationRequest.setFastestInterval(LOCATION_UPDATE_FASTEST_INTERVAL);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        }
    }

    @Override
    public void onBackPressed() {
        if (behavior.getState() != BottomSheetBehaviorWithAnchor.STATE_COLLAPSED) {
            behavior.setState(BottomSheetBehaviorWithAnchor.STATE_COLLAPSED);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onLocationChanged(Location location) {
        moveToCurrentLocation(location);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (PERMISSIONS_REQUEST_ACCESS_LOCATION == requestCode) {
            if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                enableCurrentLocation();
            }
        } else {
            // permission denied, boo! Disable the
            // functionality that depends on this permission.
        }
    }

    // ======================== UI Helper Methods ====================

    private void initBottomsheetBehavior() {
        behavior = BottomSheetBehaviorWithAnchor.from(bottomSheet);
        behavior.addBottomSheetCallback(new BottomSheetBehaviorWithAnchor.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehaviorWithAnchor.STATE_COLLAPSED:
                        if (map != null && !map.getUiSettings().isScrollGesturesEnabled()) {
                            map.getUiSettings().setScrollGesturesEnabled(true);
                        }
                        dimToolbarGroup(DIM_START_OFFSET);
                        break;
                    case BottomSheetBehaviorWithAnchor.STATE_DRAGGING:
                        if (map != null && map.getUiSettings().isScrollGesturesEnabled()) {
                            map.getUiSettings().setScrollGesturesEnabled(false);
                        }
                        break;
                    case BottomSheetBehaviorWithAnchor.STATE_EXPANDED:
                        break;
                    case BottomSheetBehaviorWithAnchor.STATE_ANCHOR_POINT:
                        break;
                    case BottomSheetBehaviorWithAnchor.STATE_HIDDEN:
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                dimBackground(slideOffset);
                dimToolbarGroup(slideOffset);

                setTitlePosition();
            }
        });
    }

    private void dimBackground(float slideOffset) {
        if (slideOffset >= 0 && slideOffset <= 1) {
            dimmedFrame.setAlpha(Math.min(slideOffset * 2f, 1f));
        }
    }

    private void dimToolbarGroup(float slideOffset) {
        float alpha = ensureAlphaWithinRange((slideOffset - DIM_START_OFFSET) * 5f);
        toolbarGroup.setAlpha(alpha);
    }

    private void setTitlePosition() {
        int translationY = bottomSheet.getTop() - anchorHeight;
        if (translationY <= 0) {
            title.setTranslationY(translationY);
        }

        float part = (float) translationY / (float) anchorHeight;
        int translationX = (int) (part * titleRightMargin);
        if (translationX <= 0) {
            title.setTranslationX(-translationX);
        }
    }

    private float ensureAlphaWithinRange(float alpha) {
        if (alpha < 0) {
            return 0f;
        } else if (alpha > 1) {
            return 1f;
        } else {
            return alpha;
        }
    }

    // ======================== Location Helper Methods ====================

    protected synchronized GoogleApiClient buildGoogleApiClient() {
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private boolean requestPermissionsIfRequired() {
        if (!checkLocationAccessEnabled()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user
            } else {
                ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_ACCESS_LOCATION);
            }
            return true;
        }
        return false;
    }

    @SuppressWarnings({"MissingPermission"})
    private void enableCurrentLocation() {
        map.setMyLocationEnabled(true);

        googleApiClient = buildGoogleApiClient();
        googleApiClient.connect();
    }

    private boolean checkLocationAccessEnabled() {
        return ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) == PERMISSION_GRANTED;
    }

    private void moveToCurrentLocation(@NonNull Location location) {
        LatLng current = new LatLng(location.getLatitude(), location.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder().target(current).zoom(14).build();
        map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

}
